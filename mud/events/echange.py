from .event import Event4

class EchangeEvent(Event4):
	NAME="echange"
	def perform(self):
		if self.object3 not in self.actor.container().contents():
			self.fail()
			return self.inform("echange.fail-exist-pnj")
		if self.object not in self.actor:
			self.fail()
			return self.inform("echange.fail-inventaire")
		if self.object2 not in self.object3.contents():
			self.fail()
			return self.inform("echange.fail-pnj")
		if not self.object.has_prop("echangeable"):
			self.fail()
			return self.inform("echange.fail-obj-un")
		if not self.object.has_prop("echangeable"):
			self.fail()
			return self.inform("echange.fail-obj-deux")
		self.object2.add_prop("takable")
		self.object.remove_prop("takable")
		self.object.move_to(self.object3)
		self.object2.move_to(self.actor)
		self.inform("echange")
