from .event import Event2

class RamerEvent(Event2):
	NAME = "ramer"

	def perform(self):
		if not self.actor.container().has_prop("bateau"):
			self.fail()
			return self.inform("ramer.fail-bateau")
		if not self.object.has_prop("ramer"):
			self.fail()	
			return self.inform("ramer.fail-rame")			
		self.inform("ramer")
