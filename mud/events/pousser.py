from .event import Event2

class PousserEvent(Event2):
    NAME = "pousser"

    def perform(self):
        if not self.object.has_prop("pousser"):
            self.fail()
            return self.inform("pousser.failed")
        self.inform("pousser")
