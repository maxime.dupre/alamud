from .event import Event3

class TuerAvecEvent(Event3):
	NAME="tuer"
	
	def perform(self):
		if not self.object.has_prop("tuable"):
			self.fail()
			return self.inform("tuer.impossible")
		if self.object2.has_prop("enflamme"):
			self.object.add_prop("cendre")
		elif not self.object2.has_prop("arme-feu"):
			self.fail()
			return self.inform("tuer.fail-weapon")
		self.object.remove_prop("tuable")
		self.inform("tuer")
			
			
