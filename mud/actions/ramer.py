from .action import Action2
from mud.events import RamerEvent

class RamerWithAction(Action2):
    EVENT = RamerEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "ramer"
