from .action import Action4
from mud.events import EchangeEvent

class EchangeAction(Action4):
	EVENT= EchangeEvent
	RESOLVE_OBJECT="resolve_for_use"
	RESOLVE_OBJECT2="resolve_for_take"
	RESOLVE_OBJECT3="resolve_for_operate"
	ACTION="echange"
