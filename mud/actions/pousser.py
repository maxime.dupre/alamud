from .action import Action2
from mud.events import PousserEvent


class PousserAction(Action2):
    EVENT = PousserEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "pousser"
